<chapter xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xi="http://www.w3.org/2001/XInclude" version="5.0" xml:lang="hu">
    <info>
        <title>Helló, Liskov!</title>
        <keywordset>
            <keyword/>
        </keywordset>
    </info>

    <section>
        <title>Liskov helyettesítés sértése</title>
        <para><emphasis>Írjunk olyan OO, leforduló Java és C++ kódcsipetet, amely megsérti a Liskov
                elvet! Mutassunk rá a megoldásra: jobb OO tervezés.</emphasis></para>
        <para><link xlink:href="https://arato.inf.unideb.hu/batfai.norbert/PROG2/Prog2_1.pdf"/>
            <emphasis>(93-99 fólia)</emphasis></para>
        <para><emphasis>(számos példa szerepel az elv megsértésére az UDPROG repóban, lásd pl.
                source/binom/Batfai-</emphasis></para>
        <para><emphasis>Barki/madarak/)</emphasis></para>
        <para> Megoldás forrása: <link
                xlink:href="https://sourceforge.net/p/udprog/code/ci/master/tree/source/binom/Batfai-Barki/madarak/"
            /></para>
        <para><link xlink:href="https://github.com/zlaval/blog/wiki/L%C3%A9gy-SOLID"/></para>
        <para><link xlink:href="https://github.com/csengetoth/liskov"/></para>
        <para> Tanulságok, tapasztalatok, magyarázat... </para>
        <para>Liskov helyettesítési elv:</para>
        <para>Célja, hogy egy osztály legyen helyettesíthető a leszármazott osztályával anélkül,
            hogy a szoftver helyes működése megváltozna.</para>
        <para>Az elv: "Ha S altípusa T-nek, akkor egy T típusú objektum helyettesíthető S típussal
            anélkül, hogy megváltozna a program helyessége".</para>
        <para>Ehhez kötődik továbbá a S.O.L.I.D. objektum orientált tervezési elvek. A SOLID egy
            betűszó, mely az objektumorientált programozás öt alapelvének kezdőbetűjéből áll össze.
            Ha ezeket az elveket betartjuk, akkor könnyen karbantartható rendszereket
            fejleszthetünk. A Liskov ennek az 5 elvnek az egyike, ő az L betű a betűszóban. </para>
        <para>Számomra segített a megértésben Bátfai Norbert tanár úr diasorában található
            kép:</para>
        <informalfigure>
            <mediaobject>
                <imageobject>
                    <imagedata fileref="img/liskov_elv.png"/>
                </imageobject>
            </mediaobject>
        </informalfigure>
        <para> A jelenség bemutatására a feladat azt kéri, hogy írjunk egy Java és egy C++
            kódcsipetet, ami megsérti a Liskov elvet. Ehhez az UDPROG repó
            source/binom/Batfai-Barki/madarak/ mappában található két C++ forráskódot használtam
            fel, majd átírtam Java-ra a Liskov elvet sértő kódcsipetet. Annyit kellett módosítani,
            hogy a Java-ban az osztály kiterjesztését az extends kulcsszóval tegyük meg, ne a :
            karakterrel,továbbá a &amp; jeleket sem szabad a kódban hagyni, mivel Java-ban minden
            referenciaként van értelmezve. Egy kis apró kiegészítést, kiíratást tettem a Madar
            osztály repul metódusába, egy kiíratást, hogy lássuk a program futásánál a problémát.
            Ezt a kód elemzésénél lentebb részletezem.</para>
        <para>Az elvet sértő C++-os kódcsipet pedig a következő:</para>
        <programlisting language="c"><![CDATA[
// ez a T az LSP-ben
class Madar {
public:
     virtual void repul() {};
};

// ez a két osztály alkotja a "P programot" az LPS-ben
class Program {
public:
     void fgv ( Madar &madar ) {
          madar.repul();
     }
};

// itt jönnek az LSP-s S osztályok
class Sas : public Madar
{};

class Pingvin : public Madar // ezt úgy is lehet/kell olvasni, hogy a pingvin tud repülni
{};

int main ( int argc, char **argv )
{
     Program program;
     Madar madar;
     program.fgv ( madar );

     Sas sas;
     program.fgv ( sas );

     Pingvin pingvin;
     program.fgv ( pingvin ); // sérül az LSP, mert a P::fgv röptetné a Pingvint, ami ugye lehetetlen.

}




]]>
        </programlisting>
        <para>A repóban a kommentezések segítettek a megértésben, ezért azokat benne hagytam, így
            könnyebb követni a kódot. Mint látható van egy P osztályunk, a Program, amiben található
            az fvg metódus. Ennek a metódusnak a törzsében meghívásra kerül a Madár osztályban
            deklarált repul metódus. Ez a Madar osztály P egyik alkotó eleme, a T osztály. A másik
            alkotó elme pedig az S osztályt szimbolizáló Sas és Pingvin osztály, amik a Madar
            osztálynak a kiterjesztései. Összefoglalva a programunkban van egy sas és egy pingvin,
            amik madarak. A madarak tudnak repülni, a reptetést pedig a Program osztály végzi
            el.</para>
        <para>A main-ben példányosítunk minden osztályra, és meg is hívjuk rá a reptetést elvégző
            függvényt. És itt jön a probléma, a Liskov helyettesítési elv lényege. Mivel a pingvin
            is leszármazottja a madár osztálynak ugyanúgy, mint a sas, így ő is reptetésre kerül,
            holott a sassal ellentétben tudjuk, hogy nem igen jellemző a pingvinekre a repülés.
            :)</para>
        <para>Ezért szükséges úgy megszervezni az osztályokat, hogy ez a hiba ne történjen
            meg.</para>
        <para>A Java-s átíratásban kiírattattam a madar, a sas és a pingvin esetében is azt, hogy a
            reptetés valóban megtörtént:</para>
        <programlisting language="c"><![CDATA[
class Madar {
public
     void repul() {System.out.println("repul");};
}


class Program {
public
     void fgv ( Madar madar ) {
          madar.repul();
     }
}

class Sas extends Madar
{}

class Pingvin extends Madar 
{}

class Liskov{

	public static void main ( String[] args)
	{	
     		Program program = new Program();
     		Madar madar = new Madar();
     		program.fgv ( madar );

     		Sas sas = new Sas();
     		program.fgv ( sas );

     		Pingvin pingvin = new Pingvin();
     		program.fgv ( pingvin ); 
	}
}
]]>
        </programlisting>
        <para><inlinemediaobject>
                <imageobject>
                    <imagedata fileref="img/liskov.png"/>
                </imageobject>
            </inlinemediaobject></para>
        <para> De hogyan javítsuk ki? A repóban található másik kód, a liskovrafigyel.cpp-ben
            láthatjuk a megoldást a problémára:</para>
        <programlisting language="c"><![CDATA[
// ez a T az LSP-ben
class Madar {
//public:
//  void repul(){};
};

// ez a két osztály alkotja a "P programot" az LPS-ben
class Program {
public:
     void fgv ( Madar &madar ) {
          // madar.repul(); a madár már nem tud repülni
          // s hiába lesz a leszármazott típusoknak
          // repül metódusa, azt a Madar& madar-ra úgysem lehet hívni
     }
};

// itt jönnek az LSP-s S osztályok
class RepuloMadar : public Madar {
public:
     virtual void repul() {};
};

class Sas : public RepuloMadar
{};

class Pingvin : public Madar // ezt úgy is lehet/kell olvasni, hogy a pingvin tud repülni
{};

int main ( int argc, char **argv )
{
     Program program;
     Madar madar;
     program.fgv ( madar );

     Sas sas;
     program.fgv ( sas );

     Pingvin pingvin;
     program.fgv ( pingvin );

}
]]>
        </programlisting>
        
        <para>Mint látjuk, a megoldás az, hogy írunk egy RepuloMadar kiterjesztést a Madar
            osztályra, és a RepuloMadarban deklaráljuk a repul metódust. A Madar osztályban pedig
            kikommentezzük az eddigi példákban alkalnazott repul metódust. Így ha a madárnál
            meghívjuk a repul metódust, nem fog tudni repülni, nem lehet rá meghívni, még akkor se,
            ha egy leszármazott típusára értelmezve van a repülés. A Sas a RepuloMadar
            kiterjesztéseként fog tudni repülni, a Pingvin a Madar kiterjesztéseként viszont már
            nem.</para>
        
    </section>        

    <section>
        <title>Szülő-gyerek</title>
        <para><emphasis>Írjunk Szülő-gyerek Java és C++ osztálydefiníciót, amelyben demonstrálni
                tudjuk, hogy az ősön</emphasis></para>
        <para><emphasis>keresztül csak az ős üzenetei küldhetőek!</emphasis></para>
        <para><emphasis>Lásd fóliák!</emphasis>
            <link xlink:href="https://arato.inf.unideb.hu/batfai.norbert/PROG2/Prog2_1.pdf"/>
            <emphasis>(98. fólia)</emphasis></para>
        <para> Megoldás forrása: <link xlink:href="https://github.com/csengetoth/liskov"/></para>
        <para>
            Tanulságok, tapasztalatok, magyarázat...
        </para>
        <para>Szintén a tanár úr diasorában található képet kellett a feladat alapjának
            tekinteni:</para>
        <informalfigure>
            <mediaobject>
                <imageobject>
                    <imagedata fileref="img/dinamikus.png"/>
                </imageobject>
            </mediaobject>
        </informalfigure>
        <para>Szintén egy Java és egy C++-os példán keresztül kell bemutatnunk a feladatot,
            ezesetben a szülő és a gyerek osztály közötti kapcsolatot, az üzenetküldést kell
            kielemeznünk. Mindenekelőtt érdemes kihangsúlyozni a két nyelv különbségét. Java-ban
            minden objektum referencia, mindig dinamikus a kötés. Ellenben a C++-szal, ahol
            megkülönböztetünk referenciát, mutatót és objektumot. Dinamikus a kötés, ha az
            ősosztályban virtual kulcsszó szerepel. </para>
        <para>Amiben megegyezik a két nyelv az az, hogy Java-ban is a szülő osztály csak a saját
            üzenetét küldheti, a gyerekét nem, C++-ban is csak az ősosztály referencián keresztül
            vagy pointeren keresztül csak az ős üzenetei küldhetőek.</para>
        <para>A Java kódcsipet a következő:</para>
        <programlisting language="c"><![CDATA[
class Szulo{
	public void uzenet(){
	System.out.println("I'm the parent class");}
}

class Gyerek extends Szulo{
	public void uzenet2(){
	System.out.println("I'm the child class");}
}

class Szulogyerek{
	public static void main (String[] args){

	Szulo szulo=new Gyerek();
	szulo.uzenet();
	szulo.uzenet2();

	}
}
]]>
        </programlisting>
        <para>A kódban létrehozunk egy szülő osztályt, ami egy üzenet metódussal kiíratja a
            képernyőre, hogy ő a szülő osztály.</para>
        <para>Majd kiterjesztjük a Gyerek osztállyal, ami egy üzenet2 metódussal kiíratja, hogy ő a
            gyerek osztály.</para>
        <para>Majd definiáljuk a Szulogyerek osztályt, a main metódusban létrehozunk egy gyerek
            objektumot, majd meghívjuk a szülő metódusát, ami kiírja hogy ő a szülő osztály, majd a
            szülőn keresztül a gyerek osztályban definiált üzenetet is meghívjuk. De ez így nem fog
            lefordulni:</para>
        <para><inlinemediaobject>
                <imageobject>
                    <imagedata fileref="img/forditasi_hiba.png"/>
                </imageobject>
            </inlinemediaobject></para>
        <para>A baj a szulo.uzenet2(); sorral van. Be is teljesült a teória, miszerint a szülőnek
            hiába leszármazottja a gyerek osztály, a gyerek osztályban definiált uzenet2 metódust a
            szülő nem hívhatja meg, csak a sajátját, ezért  szulo.uzenet(); sor a helyes csak.
            Pontosan ezt jelzi nekünk a fordító is, hogy a Szülő típussal hívjuk meg, és ezzel van a
            gond. Ha kikommentezzük vagy kitöröljük, akkor már helyesen lefordul:</para>
        <para><inlinemediaobject>
                <imageobject>
                    <imagedata fileref="img/javit.png"/>
                </imageobject>
            </inlinemediaobject></para>
        <para> Most nézzük meg hogyan működik ugyanez C++-ban!</para>
        <para>A kép alapján megállapítottuk, hogy C++-ban a dinamikus kötésnél nem csak
            referenciákkal, hanem mutatókkal is dolgozunk, és nem felejtjük el a virtual kulcszóval
            definiálni a kötést az ős osztályban. A példakódban nem található virtual kulcsszó,
            azaz, nincs dinamikus kötés, ami azt jelenti, hogy a mutatókon és referenciákon
            keresztül tudunk hozzáférni az ős üzenetéhez és csak ahhoz férhetünk hozzá:</para>
        <programlisting language="c"><![CDATA[
#include<iostream>

using namespace std;


class Szulo{
	public: void uzenet(){
	cout<<"I'm the parent class"<<endl;
	}
};

void fgv ( Szulo& parent) {
	parent.uzenet();
}

class Gyerek: public Szulo{
	public: void uzenet(){
	cout<<"I'm the child class"<<endl;
	}
};

int main( int argc, char **argv){

	Szulo* szulo=new Gyerek();
	szulo->uzenet();
	
	Gyerek gyerek;
	fgv(gyerek);	
}

]]>
        </programlisting>
        <para>Hasonlóképpen létrehozunk egy szulo osztályt ugyanazzal az üzenetkiírós metódussal,
            majd definiálunk egy fgv függvényt, ami paraméterül egy Szulo referenciát vár. Szintén
            lesz egy gyerek alosztályunk, aminek az őse a szulo osztály. Metódusa pedig az előző
            példával megegyező "én vagyok  a gyerek osztály" szöveg kiírás. A main-ben helyet
            foglalunk a Gyereknek, és ráállítunk egy Szulo mutatót:</para>
        <programlisting>
Szulo* szulo=new Gyerek();           
        </programlisting>
        <para>A következő sorral pedig meghívásra kerül a szulo üzenete, szó szerint kiolvasva: a
            szulo által mutatott uzenet() hívódik meg.</para>
        <programlisting>
szulo->uzenet();        
        </programlisting>
        <para> Az utolsó két sorban meghívjuk az fvg függvényt, ami szülő típusú referenciát vár.
            Paraméterként adjuk neki a gyerek típusú objektumot, amit már előtte létrehoztunk. Ez
            eredményezi azt, hogy úgyszintén a szulo üzenete kerül kiíratásra.</para>
        <programlisting>
Gyerek gyerek;
fgv(gyerek);           
        </programlisting>
        <para>
            <inlinemediaobject>
                <imageobject>
                    <imagedata fileref="img/szulogyerek.png"/>
                </imageobject>
            </inlinemediaobject></para>
        
    </section> 
    
    <section>
        <title>Anti OO</title>
        <para><emphasis>A BBP algoritmussal a Pi hexadecimális kifejtésének a 0. pozíciótól
                számított 10^6, 10^7, 10^8 darab jegyét határozzuk meg C, C++, Java és C# nyelveken
                és vessük össze a futási időket!</emphasis></para>
        <para><link xlink:href="https://www.tankonyvtar.hu/hu/tartalom/tkt/javat-tanitok-javat/apas03.html#id561066"/>
        </para>
        <para> Megoldás forrása: <link xlink:href="https://github.com/csengetoth/liskov"/></para>
        <para>
            Tanulságok, tapasztalatok, magyarázat...
        </para>
        <para>A feladat megoldására az előző heti Kódolás from scratch nevű feladatában használt
            PiBBP osztálynak egy egyszerűsített változatát, a PiBench osztályt használjuk. Ennek az
            osztálynak a C, C++, Java és C# ( C Sharp) átíratain kell megvizsgálnunk a futási
            időket, azaz hogy mennyi idő alatt tudja a program a Pi hexadecimális jegyeit(10^6.,
            10^7., 10^8) a 0. pozíciótól kiszámolni.</para>
        <para>A számításokat jegyenként végezzük el, azaz külön megnézzük 10^6-ra, 10^7-re és
            10^8-ra is. Ehhez szükséges mind a 4 átíratba belenyúlni és a for ciklusban
            megváltoztatni a d változó értékét:</para>
        <programlisting language="c"><![CDATA[
for (d = 1000000; d < 1000001; ++d)
    {

      d16Pi = 0.0;

      d16S1t = d16Sj (d, 1);
      d16S4t = d16Sj (d, 4);
      d16S5t = d16Sj (d, 5);
      d16S6t = d16Sj (d, 6);

      d16Pi = 4.0 * d16S1t - 2.0 * d16S4t - d16S5t - d16S6t;

      d16Pi = d16Pi - floor (d16Pi);

      jegy = (int) floor (16.0 * d16Pi);

    }
]]>
        </programlisting>
        <para> Ebben az esetben 10^6 db jegyre d-t 1000000-ra állítjuk. Ugyanígy kell majd 10^7 db
            jegy esetén is, csak akkor a d értéke 10000000 lesz, 10^8-nál pedig 100000000. Ha ezt
            nem csak C-ben, hanem C#-ban, C++-ban és Java-ban is külön külön módosítjuk, akkor egy
            táblázatban összehasonlíthatjuk a futási értékeket, amiket a processzor másodpercben
            jelenít meg.</para>
        <para>Az én gépem i5-8265U CPU @ 1.60GHz × 8 processzorral dolgozik, ezalapján a futási
            eredményeim a következők:</para>
        <para>C esetében:</para>
        <informalfigure>
            <mediaobject>
                <imageobject>
                    <imagedata fileref="img/ido_c.png"/>
                </imageobject>
            </mediaobject>
        </informalfigure>
        <para> Java esetében:</para>
        <para><inlinemediaobject>
                <imageobject>
                    <imagedata fileref="img/ido_java.png"/>
                </imageobject>
            </inlinemediaobject></para>
        <para> C++ esetében:</para>
        <para><inlinemediaobject>
                <imageobject>
                    <imagedata fileref="img/ido_c++.png"/>
                </imageobject>
            </inlinemediaobject></para>
        <para> C# esetében:</para>
        <para><inlinemediaobject>
                <imageobject>
                    <imagedata fileref="img/ido_csharp.png"/>
                </imageobject>
            </inlinemediaobject></para>
        <para> A futási eredmények másodpercben:</para>
        <para>
            <informaltable frame="all">
                <tgroup cols="5" align="center">
                    <colspec colname="c1" colnum="1" colwidth="1*"/>
                    <colspec colname="c2" colnum="2" colwidth="1*"/>
                    <colspec colname="c3" colnum="3" colwidth="1*"/>
                    <colspec colname="c4" colnum="4" colwidth="1*"/>
                    <colspec colname="c5" colnum="5" colwidth="1*"/>
                    <tbody>
                        <row>
                            <entry/>
                            <entry align="center">C</entry>
                            <entry align="center">Java</entry>
                            <entry>C++</entry>
                            <entry>C#</entry>
                        </row>
                        <row>
                            <entry>10^6</entry>
                            <entry>1.594163</entry>
                            <entry>1.372</entry>
                            <entry>1.98436</entry>
                            <entry>1.511722</entry>
                        </row>
                        <row>
                            <entry>10^7</entry>
                            <entry>18.450019</entry>
                            <entry>15.967</entry>
                            <entry>24.3569</entry>
                            <entry>17.237233</entry>
                        </row>
                        <row>
                            <entry>10^8</entry>
                            <entry>238.416794</entry>
                            <entry>204.372</entry>
                            <entry>286.731</entry>
                            <entry>217.379692</entry>
                        </row>
                    </tbody>
                </tgroup>
            </informaltable>
        </para>
        
        
    </section> 
    
    <section>
        <title>Hello, Android!</title>
        <para><emphasis>Élesszük fel az SMNIST for Humans projektet!</emphasis></para>
        <para><link
                xlink:href="https://gitlab.com/nbatfai/smnist/tree/master/forHumans/SMNISTforHumansExp3/app/src/main"
            /></para>
        <para><emphasis>Apró módosításokat eszközölj benne, pl. színvilág.</emphasis></para>
        <para> Megoldás forrása: </para>
        <para>
            Tanulságok, tapasztalatok, magyarázat...
        </para>
    </section>    
    
    <section>
        <title>Ciklomatikus komplexitás</title>
        <para><emphasis>Számoljuk ki valamelyik programunk függvényeinek ciklomatikus komplexitását!
                Lásd a fogalom tekintetében a</emphasis>
            <link
                xlink:href="https://arato.inf.unideb.hu/batfai.norbert/UDPROG/deprecated/Prog2_2.pdf"/>
            <emphasis>(77-79 fóliát)!</emphasis></para>
        <para> Megoldás forrása: <link xlink:href="http://hadmernok.hu/2009_4_kun.pdf"/> (262.
            oldal)</para>
        <para>Megoldás videó: <link xlink:href="https://youtu.be/kuK_pKdpfKg"/> </para>
        <para>A ciklomatikus komplexitás segítségével megállapíthatjuk, hogy egy adott programnak
            mennyi a komplexitása, bonyolultsága. Ez egy jó iránymutató a program fejlesztéséhez és
            teszteléséhez. Meghatározása a ciklomatikus mutatószámmal történik, amit McCabe javasolt
            a mérés elvégzésére. Kiszámításának képlete:</para>
        <para>v(G) = e - n + 2</para>
        <para>v (nű) - a ciklomatikus komplexitás</para>
        <para>e - a gráf éleinek száma</para>
        <para>n - a gráf csúcsainak száma</para>
        <para>A gráf csúcsai az utasítás-szekvenciáknak (elágazás nélküli utasítás-sorozatoknak)
            felelnek meg, élei pedig a program által a szekvenciák között létrehozott lehetséges
            közvetlen átmeneteknek.</para>
        <para>A tesztelési munkánál ez a mutató nagy segítséget nyújthat, hiszen az pontosan egyenlő
            a vezérlésfolyam-gráf független, legalább egy eltérő élt tartalmazó útjainak maximális
            számával. Így tudjuk, hogy a tesztelési útvonalak maximális számát. Ha ennél több lenne
            a tesztelés, akkor több ugyanolyan útvonal lenne.</para>
        <para>A ciklomatikus mutató a szoftver kockázati szintjéről is tájékoztatást ad. Ajánlott a
            10-nél alacsonyabb mutatószám-értékre törekedni.</para>
        <para>
            <informaltable frame="all">
                <tgroup cols="2">
                    <colspec colname="c1" colnum="1" colwidth="1*"/>
                    <colspec colname="c2" colnum="2" colwidth="1*"/>
                    <thead>
                        <row>
                            <entry>Ciklomatikus komplexitás</entry>
                            <entry>Kockázati szint</entry>
                        </row>
                    </thead>
                    <tbody>
                        <row>
                            <entry>1-10</entry>
                            <entry>Egyszerű program, kevés kockázat</entry>
                        </row>
                        <row>
                            <entry>11-20</entry>
                            <entry>Mérsékelten bonyolult program, mérsékelt kockázat</entry>
                        </row>
                        <row>
                            <entry>21-50</entry>
                            <entry>Bonyolult program, magas kockázat</entry>
                        </row>
                        <row>
                            <entry>Több mint 50</entry>
                            <entry>Nem tesztelhető program, nagyon magas kockázat</entry>
                        </row>
                    </tbody>
                </tgroup>
            </informaltable>
        </para>
        <para> A feladat szerint egy tetszőleges programnak ki kell számolnunk a ciklomatikus
            komplexitását. A tanárnőm javaslatára az online <emphasis>lizard code complexity
                analyzer</emphasis>-t (<link xlink:href="http://www.lizard.ws/"/>) hívtam
            segítségül. Használata egyszerű, ki kell választani a legördülő ablakból a
            programnyelvet, majd bemásolni a kódot. Az analyse gombra kattintás után villámgyors
            eredményt kapunk. </para>
        <para>Az előző heti feladatcsokorból a Kódolás from scratch című feladat PiBBP.java
            programnak számítottam ki a kalkulátorral a komplexitását, ezt az eredményt
            kaptam:</para>
        <informalfigure>
            <mediaobject>
                <imageobject>
                    <imagedata fileref="img/pi_komplexitas.png"/>
                </imageobject>
            </mediaobject>
        </informalfigure>
        <para> Az eredmény alapján megállapíthatjuk, hogy ez a program az első, vagyis a legjobb
            kategóriába tartozik, a legkevésbé kockázatos.</para>
        
    </section>    
    
    <section>
        <title>EPAM: Interfész evolúció Java-ban</title>
        <para><emphasis>Mutasd be milyen változások történtek Java 7 és Java 8 között az
                interfészekben. Miért volt erre szükség, milyen problémát vezetett ez
            be?</emphasis></para>
        <para> Megoldás forrása: <link
                xlink:href="https://github.com/csengetoth/epam-deik-prog2/tree/master/week-2/interfaces/src/main/java/com/epam/training"
            /></para>
        <para>
            Tanulságok, tapasztalatok, magyarázat...
        </para>
        <para>A Java 8 után lehetővé vált az interfészekben az úgynevezett, default metódus
            implementációk megadása. Azért volt erre szükség, mert lehetővé tette a visszafele
            kompatibilitást az ebben a verzióban megjelent újításokkal kapcsolatban.</para>
        <para>A probléma amit bevezetett, az a többszörös öröklődés problémája. Ugyanis a default
            implementation ellenére is a programozó kénytelen explicit módon felüldefiniálást
            végrehajtania, az egyértelműség érdekében.</para>
        <para>Az EPAM-os repóban van egy példakód, amin keresztül be lehet mutatni a
            problémát:</para>
        <programlisting language="c"><![CDATA[
package com.epam.training;

public interface InterfaceA {

	default public void method() {
		System.out.println("InterfaceA.method()");
	}
	
}

]]>
        </programlisting>
        <para>Létrehozunk egy A interfészt, és alkalmazzuk is a default metódus implementációt, majd
            kiírattatjuk a metódus nevét.</para>
        <programlisting language="c"><![CDATA[
package com.epam.training;

public interface InterfaceB {

	default public void method() {
		System.out.println("InterfaceB.method()");
	}
	
}

]]>
        </programlisting>
        <para> Ugyanígy egy B interfészt is megalkotunk.</para>
        <programlisting language="c"><![CDATA[
public class Implementation implements InterfaceA, InterfaceB {

	@Override
	public void method() {
		InterfaceA.super.method();
	}

}

]]>
        </programlisting>
        <para>Így mind a két interfész rendelkezik így egy method nevezetű metódussal, amire
            vonatkozóan default implementációt biztosítanak.</para>
        <para>Az Implementation osztályban implementáljuk a két interfészt, de látható, hogy
            mindazok ellenére, hogy létezik default implementáció a metódusra, mégis explicit módon
            felül kell definiálni a method metódust és meghatározni, hogy ezesetben az A default
            implementációját szeretnénk használni.</para>
        <para> </para>
    </section>

    <section>
        <title>EPAM: Liskov féle helyettesíthetőség elve, öröklődés</title>
        <para><emphasis>Adott az alábbi osztály hierarchia.</emphasis></para>
        <para>class Vehicle, class Car extends Vehicle, class Supercar extends</para>
        <para>Car</para>
        <para><emphasis>Mindegyik osztály konstruktorában történik egy kiíratás, valamint a Vehicle
                osztályban szereplő start() metódus mindegyik alosztályban felül van
                definiálva.</emphasis></para>
        <para><emphasis>Mi történik ezen kódok futtatása esetén, és miért?</emphasis></para>
        <para>Vehicle firstVehicle = new Supercar();</para>
        <para>firstVehicle.start();</para>
        <para>System.out.println(firstVehicle instanceof Car);</para>
        <para>Car secondVehicle = (Car) firstVehicle;</para>
        <para>secondVehicle.start();</para>
        <para>System.out.println(secondVehicle instanceof Supercar);</para>
        <para>Supercar thirdVehicle = new Vehicle();</para>
        <para>thirdVehicle.start();</para>
        <para>
            Megoldás videó:
        </para>
        <para> Megoldás forrása: </para>
        <para>
            Tanulságok, tapasztalatok, magyarázat...
        </para>
        <para> </para>
    </section> 
    
    <section>
        <title>EPAM: Interfész, Osztály, Absztrak Osztály</title>
        <para><emphasis>Mi a különbség Java-ban a Class, Abstract Class és az Interface között? Egy
                tetszőleges példával /példa kódon keresztül mutasd be őket és hogy mikor melyik
                koncepciót célszerű használni.</emphasis></para>
        <para>
            Megoldás videó:
        </para>
        <para> Megoldás forrása: </para>
        <para>
            Tanulságok, tapasztalatok, magyarázat...
        </para>
        <para> </para>
    </section>
        
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
</chapter>                
